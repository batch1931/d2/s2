package com.zuitt.batch193;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class ArrayTest {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        int[] primeNumbers = {2, 3, 5, 7, 11}; //create an Array of prime numbers

        System.out.println("Enter the index (1-5): ");
        int number = appScanner.nextInt();

        switch (number) {
            case 1 -> System.out.println("The prime number is " + primeNumbers[0]);
            case 2 -> System.out.println("The prime number is " + primeNumbers[1]);
            case 3 -> System.out.println("The prime number is " + primeNumbers[2]);
            case 4 -> System.out.println("The prime number is " + primeNumbers[3]);
            case 5 -> System.out.println("The prime number is " + primeNumbers[4]);
            default -> System.out.println("Invalid Index");
        }

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Jay"));

        System.out.println("My friends are: " + friends.toString());

        HashMap<String, Integer> inventory = new HashMap<>();
            inventory.put("toothpaste", 15);
            inventory.put("toothbrush", 20);
            inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);

    }

}
